Teensy LC
=========

This crate is an incomplete HAL and board crate for the
[Teensy LC](https://www.pjrc.com/teensy/teensyLC.html) microcontroller
board.

Beware: it's early stage and very much a work in progress. It might
never support more than what I directly need for my projects (but
patches welcome).

Currently requires nightly (for never type and inline asm in
cortex-m-rt crate).

Usage
-----

See [examples](./examples/). `cargo build` will generate an ELF
binary. To convert it to hex format suitable for the
[loader CLI](https://github.com/PaulStoffregen/teensy_loader_cli), run:

    objcopy -O ihex path/to/elf_binary path/to/hex_file

To convert it to raw binary which you can hexdump and inspect, run:

    objcopy -O binary path/to/elf_binary path/to/hex_file
