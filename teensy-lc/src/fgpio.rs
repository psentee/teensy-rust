use teensy_lc_macros::fgpio;

pub trait FgpioExt {
    type Parts;

    fn split(self) -> Self::Parts;
}

// typestate structs
pub struct IN;
pub struct OUT;

fgpio!(a);
fgpio!(b);
fgpio!(c);
fgpio!(d);
fgpio!(e);
