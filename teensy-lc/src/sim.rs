use embedded_hal::watchdog::{Watchdog, WatchdogDisable, WatchdogEnable};

use crate::dev::sim;

pub struct Srvcop<'a>(pub &'a sim::SRVCOP);

impl<'a> Watchdog for Srvcop<'a> {
    fn feed(&mut self) {
        self.0.write(|w| unsafe { w.srvcop().bits(0x55) });
        self.0.write(|w| unsafe { w.srvcop().bits(0xAA) });
    }
}

pub struct Copc<'a>(pub &'a sim::COPC);

#[repr(u32)]
pub enum COPTime {
    /// Disable COP
    Never = 0,

    /// 32 cycles of internal 1kHz clock
    LPO32ms = 0b0100,

    /// 256 cycles of internal 1kHz clock
    LPO256ms = 0b1000,

    /// 1024 cycles of internal 1kHz clock, default state
    LPO1024ms = 0b1100,

    /// 8192 bus clock cycles, ~170µs at 48MHz
    Bus2e13 = 0b0110,

    /// 65536 bus clock cycles, ~1.36ms at 48MHz
    Bus2e16 = 0b1010,

    /// 262144 bus clock cycles, ~5.46ms at 48MHz
    Bus2e18 = 0b1110,

    /// 8192 bus clock cycles, ~170µs at 48MHz, windowed mode
    Bus2e13W = 0b0111,

    /// 65536 bus clock cycles, ~1.36ms at 48MHz, windowed mode
    Bus2e16W = 0b1011,

    /// 262144 bus clock cycles, ~5.46ms at 48MHz, windowed mode
    Bus2e18W = 0b1111,
}

impl<'a> WatchdogEnable for Copc<'a> {
    type Time = COPTime;

    fn start<T>(&mut self, period: T)
    where
        T: Into<Self::Time>,
    {
        self.0.write(|w| unsafe { w.bits(period.into() as u32) });
    }
}

impl<'a> WatchdogDisable for Copc<'a> {
    fn disable(&mut self) {
        // self.0.write(|w| w.copt()._00());
        self.start(COPTime::Never);
    }
}
