use core::panic::PanicInfo;

use crate::delay_usec;

const TIMES: [(u32, u32); 9] = [
    (100_000, 100_000),
    (100_000, 100_000),
    (100_000, 750_000),
    (500_000, 100_000),
    (500_000, 100_000),
    (500_000, 750_000),
    (100_000, 100_000),
    (100_000, 100_000),
    (100_000, 750_000 * 3),
];

#[inline(never)]
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    let sim = unsafe { &(*crate::dev::SIM::ptr()) };
    let portc = unsafe { &(*crate::dev::PORTC::ptr()) };
    let fgpioc = unsafe { &(*crate::dev::FGPIOC::ptr()) };

    sim.scgc5.write(|w| w.portc().set_bit());
    portc.pcr5.write(|w| w.mux()._001());
    fgpioc.pddr.write(|w| unsafe { w.bits(1 << 5) });

    loop {
        for (high_us, low_us) in &TIMES {
            fgpioc.psor.write(|w| unsafe { w.bits(1 << 5) });
            delay_usec(*high_us);
            fgpioc.pcor.write(|w| unsafe { w.bits(1 << 5) });
            delay_usec(*low_us);
        }
    }
}
