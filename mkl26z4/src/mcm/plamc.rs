#[doc = "Reader of register PLAMC"]
pub type R = crate::R<u16, super::PLAMC>;
#[doc = "Possible values of the field `AMC`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum AMC_A {
    #[doc = "A bus master connection to AXBS input port n is absent"]
    _0,
    #[doc = "A bus master connection to AXBS input port n is present"]
    _1,
}
impl crate::ToBits<u8> for AMC_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            AMC_A::_0 => 0,
            AMC_A::_1 => 1,
        }
    }
}
#[doc = "Reader of field `AMC`"]
pub type AMC_R = crate::R<u8, AMC_A>;
impl AMC_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> crate::Variant<u8, AMC_A> {
        use crate::Variant::*;
        match self.bits {
            0 => Val(AMC_A::_0),
            1 => Val(AMC_A::_1),
            i => Res(i),
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline(always)]
    pub fn is_0(&self) -> bool {
        *self == AMC_A::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline(always)]
    pub fn is_1(&self) -> bool {
        *self == AMC_A::_1
    }
}
impl R {
    #[doc = "Bits 0:7 - Each bit in the AMC field indicates whether there is a corresponding connection to the AXBS master input port."]
    #[inline(always)]
    pub fn amc(&self) -> AMC_R {
        AMC_R::new((self.bits & 0xff) as u8)
    }
}
