#[doc = "Reader of register PDIR"]
pub type R = crate::R<u32, super::PDIR>;
#[doc = "Possible values of the field `PDI`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum PDI_A {
    #[doc = "Pin logic level is logic 0, or is not configured for use by digital function."]
    _0,
    #[doc = "Pin logic level is logic 1."]
    _1,
}
impl crate::ToBits<u32> for PDI_A {
    #[inline(always)]
    fn _bits(&self) -> u32 {
        match *self {
            PDI_A::_0 => 0,
            PDI_A::_1 => 1,
        }
    }
}
#[doc = "Reader of field `PDI`"]
pub type PDI_R = crate::R<u32, PDI_A>;
impl PDI_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> crate::Variant<u32, PDI_A> {
        use crate::Variant::*;
        match self.bits {
            0 => Val(PDI_A::_0),
            1 => Val(PDI_A::_1),
            i => Res(i),
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline(always)]
    pub fn is_0(&self) -> bool {
        *self == PDI_A::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline(always)]
    pub fn is_1(&self) -> bool {
        *self == PDI_A::_1
    }
}
impl R {
    #[doc = "Bits 0:31 - Port Data Input"]
    #[inline(always)]
    pub fn pdi(&self) -> PDI_R {
        PDI_R::new((self.bits & 0xffff_ffff) as u32)
    }
}
