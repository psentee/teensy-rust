#[doc = "Reader of register FCFG2"]
pub type R = crate::R<u32, super::FCFG2>;
#[doc = "Reader of field `MAXADDR1`"]
pub type MAXADDR1_R = crate::R<u8, u8>;
#[doc = "Reader of field `MAXADDR0`"]
pub type MAXADDR0_R = crate::R<u8, u8>;
impl R {
    #[doc = "Bits 16:22 - This field concatenated with leading zeros plus the value of the MAXADDR1 field indicates the first invalid address of the second program flash block (flash block 1)"]
    #[inline(always)]
    pub fn maxaddr1(&self) -> MAXADDR1_R {
        MAXADDR1_R::new(((self.bits >> 16) & 0x7f) as u8)
    }
    #[doc = "Bits 24:30 - Max Address lock"]
    #[inline(always)]
    pub fn maxaddr0(&self) -> MAXADDR0_R {
        MAXADDR0_R::new(((self.bits >> 24) & 0x7f) as u8)
    }
}
