#[doc = "Reader of register RCR3"]
pub type R = crate::R<u32, super::RCR3>;
#[doc = "Writer for register RCR3"]
pub type W = crate::W<u32, super::RCR3>;
#[doc = "Register RCR3 `reset()`'s with value 0"]
impl crate::ResetValue for super::RCR3 {
    type Type = u32;
    #[inline(always)]
    fn reset_value() -> Self::Type {
        0
    }
}
#[doc = "Reader of field `WDFL`"]
pub type WDFL_R = crate::R<bool, bool>;
#[doc = "Write proxy for field `WDFL`"]
pub struct WDFL_W<'a> {
    w: &'a mut W,
}
impl<'a> WDFL_W<'a> {
    #[doc = r"Sets the field bit"]
    #[inline(always)]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r"Clears the field bit"]
    #[inline(always)]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bit(self, value: bool) -> &'a mut W {
        self.w.bits = (self.w.bits & !0x01) | ((value as u32) & 0x01);
        self.w
    }
}
#[doc = "Possible values of the field `RCE`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum RCE_A {
    #[doc = "Receive data channel N is disabled."]
    _0,
    #[doc = "Receive data channel N is enabled."]
    _1,
}
impl crate::ToBits<bool> for RCE_A {
    #[inline(always)]
    fn _bits(&self) -> bool {
        match *self {
            RCE_A::_0 => false,
            RCE_A::_1 => true,
        }
    }
}
#[doc = "Reader of field `RCE`"]
pub type RCE_R = crate::R<bool, RCE_A>;
impl RCE_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> RCE_A {
        match self.bits {
            false => RCE_A::_0,
            true => RCE_A::_1,
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline(always)]
    pub fn is_0(&self) -> bool {
        *self == RCE_A::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline(always)]
    pub fn is_1(&self) -> bool {
        *self == RCE_A::_1
    }
}
#[doc = "Write proxy for field `RCE`"]
pub struct RCE_W<'a> {
    w: &'a mut W,
}
impl<'a> RCE_W<'a> {
    #[doc = r"Writes `variant` to the field"]
    #[inline(always)]
    pub fn variant(self, variant: RCE_A) -> &'a mut W {
        use crate::ToBits;
        {
            self.bit(variant._bits())
        }
    }
    #[doc = "Receive data channel N is disabled."]
    #[inline(always)]
    pub fn _0(self) -> &'a mut W {
        self.variant(RCE_A::_0)
    }
    #[doc = "Receive data channel N is enabled."]
    #[inline(always)]
    pub fn _1(self) -> &'a mut W {
        self.variant(RCE_A::_1)
    }
    #[doc = r"Sets the field bit"]
    #[inline(always)]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r"Clears the field bit"]
    #[inline(always)]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r"Writes raw bits to the field"]
    #[inline(always)]
    pub fn bit(self, value: bool) -> &'a mut W {
        self.w.bits = (self.w.bits & !(0x01 << 16)) | (((value as u32) & 0x01) << 16);
        self.w
    }
}
impl R {
    #[doc = "Bit 0 - Word Flag Configuration"]
    #[inline(always)]
    pub fn wdfl(&self) -> WDFL_R {
        WDFL_R::new((self.bits & 0x01) != 0)
    }
    #[doc = "Bit 16 - Receive Channel Enable"]
    #[inline(always)]
    pub fn rce(&self) -> RCE_R {
        RCE_R::new(((self.bits >> 16) & 0x01) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Word Flag Configuration"]
    #[inline(always)]
    pub fn wdfl(&mut self) -> WDFL_W {
        WDFL_W { w: self }
    }
    #[doc = "Bit 16 - Receive Channel Enable"]
    #[inline(always)]
    pub fn rce(&mut self) -> RCE_W {
        RCE_W { w: self }
    }
}
